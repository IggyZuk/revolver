public static class Config
{
	public const int BULLET_LIFE_TICKS = 100;
	public const int MAGAZINE_SIZE = 6;
	public const int DEFAULT_BANDIT_TURNS = 8;
	public const int SPAWN_BANDIT_COUNT = 32;
    public const int PREDICTION_STEPS = 5;
	public const float WIND_STRENGTH = 0f;
    public const float VIEW_LERP = 10f;
    public const float SPREAD = 0.25f;
    public const float SPAWN_RANGE = 15f;
    public const float MIN_DISTANCE = 5f; 
}